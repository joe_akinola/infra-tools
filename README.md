### Helm Values Configuration and Deployment Steps
To configure Helm values and deploy your applications, follow these steps for each of the charts: gitlab-runner and node-app.

##### Configuring Helm Values

1. Gitlab-runner
For the Gitlab-runner chart, you need to provide your GitLab token. Follow these steps:

* Locate the Helm chart values file for Gitlab-runner, typically named values.yaml.

* Open the values.yaml file in a text editor.

* Look for a section related to GitLab configuration. This section may have a field named gitlabToken or similar.

* Set the gitlabToken field to your GitLab token. It should look something like this:
* youll need to generate this token form gitlab: follow this installation guide https://docs.gitlab.com/runner/install/kubernetes.html

```
gitlabToken: YOUR_GITLAB_TOKEN_HERE
```
Save the values.yaml file.

2. Node-app
For the Node-app chart, you need to configure the following:

* Image name and tag
* Port number
* Desired replica count
* Ingress configuration

##### Follow these steps:

* Locate the Helm chart values file for Node-app, typically named values.yaml.

* Open the values.yaml file in a text editor.

* Look for sections related to Node-app configuration. You may find fields like image.repository, image.tag, service.port, replicaCount, and ingress in this file.

* Configure the following fields as per your requirements:

* image.repository: Set the Docker image repository and name.

* image.tag: Specify the Docker image tag.

* service.port: Define the port number for your application.

* replicaCount: Set the desired number of replicas.

* ingress: Configure the Ingress resource with the necessary host and paths. Here's an example:
```
ingress:
  enabled: true
  hosts:
    - host: your.domain.com
      paths:
        - /
```
Save the values.yaml file.



#### Deployment Steps
After configuring the Helm values, proceed with the deployment using the provided scripts:

1. Deploy.sh Script
Run the deploy.sh script to install the necessary applications. If you are deploying the applications for the first time, uncomment any relevant sections in the script (e.g., helm pull section) as mentioned in your instructions.


```
./deploy.sh
```
This script will use Helm to deploy the configured charts, including Gitlab-runner and Node-app.

2. Test.sh Script
After the installation, use the test.sh script to test if your installations were successful. This script should verify that your applications are running as expected.

``````
./test.sh
``````

Follow these steps for both Gitlab-runner and Node-app charts to ensure that your Helm values are correctly configured, and your applications are successfully deployed and tested.

