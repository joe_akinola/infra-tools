#!/usr/bin/env bash

########### install metrics server for HPA, to test the replica autoscaling #############

kubectl create ns metric-server
# helm repo add metrics-server https://kubernetes-sigs.github.io/metrics-server/   ##optional if new deployment 
# helm pull  metrics-server/metrics-server --version 3.11.0 --untar   ###optional
helm install metrics-server metrics-server/. --version 3.11.0 -n metrics-server
kubectl  get all -n metrics-server
kubectl top nodes


########## install ingress-controller for internal traffic and to access the node app on web browser ########
kubectl create ns ingress-nginx
# helm repo add bitnami https://charts.bitnami.com/bitnami ##optional if new deployment
# helm pull bitnami/nginx-ingress-controller --version 9.7.9 --untar  ##optional 
helm install ingress-nginx nginx-ingress-controller/. -n ingress-nginx
kubectl get all -n ingress-nginx


######### install gitlab-runner optional for cicd ###############
kubectl create ns gitlab-runner
# helm repo add bitnami https://charts.bitnami.com/bitnami ##optional only for  new deployment
# helm pull  gitlab/gitlab-runner --version 0.48.0 --untar  ##optional 
helm install gitlab-runner gitlab-runner/. -n gitlab-runner
kubectl get all -n ingress-nginx

kubectl create ns webservice
helm install node-app node-app/. -n webservice 

