#!/bin/bash

# List of namespaces to check
namespaces=("ingress-nginx" "gitlab-runner" "webservice")

# Function to check if all pods are running in a specific namespace
wait_for_pods() {
  local namespace="$1"
  local expected_count="$2"

  echo "Waiting for all pods in namespace $namespace to be running..."
  
  # Add an empty line for better separation

  # Set a maximum waiting time (in seconds)
  max_wait_time=300  # Adjust as needed (e.g., 10 to 30 seconds)

  # Calculate the end time
  end_time=$((SECONDS + max_wait_time))

  while [ $SECONDS -lt $end_time ]; do
    actual_count=$(kubectl get pods -n "$namespace" --field-selector=status.phase=Running --no-headers=true | wc -l)

    if [ "$actual_count" -eq "$expected_count" ]; then
      break
    fi

    sleep 5
  done

  if [ "$actual_count" -ne "$expected_count" ]; then
    echo "Timeout: All pods in namespace $namespace are not running."
    exit 1
  fi

  echo "All pods in namespace $namespace are running."
  
  # Add an empty line for better separation

}

# Iterate through the namespaces and check pod status
for namespace in "${namespaces[@]}"; do
  # Get the number of expected pods in the namespace (adjust as needed)
  if [ "$namespace" == "ingress-nginx" ]; then
    expected_count=1
  elif [ "$namespace" == "gitlab-runner" ]; then
    expected_count=1
  elif [ "$namespace" == "webservice" ]; then
    expected_count=1
  else
    expected_count=0
  fi

  # Check if the namespace exists
  if kubectl get ns "$namespace" >/dev/null 2>&1; then
    # Create the namespace if it doesn't exist
    echo "-----Namespace $namespace already exists.------"
  else
    kubectl create ns "$namespace"
    echo "Created namespace $namespace."
  fi

  # Wait for all pods in the namespace to be running
  wait_for_pods "$namespace" "$expected_count"
  
  # Check if kubectl get all was successful
  if [ $? -eq 0 ]; then
    echo "kubectl get all -n $namespace was successful."
  else
    echo "kubectl get all -n $namespace failed. Exiting."
    exit 1
  fi
  
  # Add an empty line for better separation
  echo
  echo
  echo
done
